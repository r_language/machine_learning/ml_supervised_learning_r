***Objectives*** 

Use binary tree decision then validate models with supervized learning, cross validation and ROC curve.

**Step 1**

Loading required packages, libraries.

**Step 2**

Get to know the data.

**Step 3**

Build a tree on the data to explain the kyphosis variable.

**Step 4**

Build another tree.

**Step 5**

Class Prediction vs Class Probability : allows to trace the ROC curve.

**Step 6**

Evaluation on the learning data.

**Step 7**

Build a tree and calculate true positive, false positive, true negative and false negative.


***The Naive Bayesian Classifier***

**Step 1**

Load the different libraries, packages.

**Step 2**

Evaluation of the classifier with ROC curve.

**Step 3**

ROC curve with a cross validation.


Author Marion Estoup

E-mail : marion_110@hotmail.fr

February 2021



